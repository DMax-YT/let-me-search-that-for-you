import { createRouter, createWebHashHistory } from "vue-router";

const routes = [
  {
    path: "/",
    name: "Home",
    component: () => import("../views/Home.vue"),
  },
  {
    path: "/duckduckgo",
    name: "DuckDuckGo",
    component: () => import("../views/DuckDuckGo.vue"),
  },
  {
    path: "/google",
    name: "Google",
    component: () => import("../views/Google.vue"),
  },
  {
    path: "/yandex",
    name: "yandex",
    component: () => import("../views/Yandex.vue"),
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
