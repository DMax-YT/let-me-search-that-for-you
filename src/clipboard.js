export const copy = (data) => {
  if (window.navigator?.clipboard) {
    window.navigator.clipboard.writeText(data);
  } else {
    const textArea = document.createElement("textarea");
    textArea.style.top = "0";
    textArea.style.left = "0";
    textArea.style.position = "fixed";
    textArea.value = data;
    document.body.appendChild(textArea);

    textArea.focus();
    textArea.select();
    document.execCommand("copy");

    document.body.removeChild(textArea);
  }
};
